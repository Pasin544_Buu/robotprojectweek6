/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.robotprojectweek6;

import java.util.Scanner;

/**
 *
 * @author Pla
 */
public class MainProgram {
    public static void main(String[] args) {
       Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10,10);
        Robot robot = new Robot(2,2,'x',map);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true){
            map.showMap();
            // W=a N=w E=d S=s q=out
            char direction = inputDirection(kb);
            if(direction=='q'){
                printBye();
                break;
            }
            robot.walk(direction);
            
        }
    }

    private static void printBye() {
        System.out.println("Bye Have A Good Day");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
